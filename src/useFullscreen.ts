import { useEffect, useState } from 'react';

const doc = document;

const changeEvent = [
  'fullscreenchange',
  'webkitfullscreenchange',
  'mozfullscreenchange',
  'MSFullscreenChange',
].find(name => `on${name}` in doc);

const fullscreenEnabled =
  changeEvent &&
  doc.fullscreenEnabled !== false &&
  (doc as any).webkitFullscreenEnabled !== false &&
  (doc as any).mozFullScreenEnabled !== false &&
  (doc as any).msFullscreenEnabled !== false;

function requestFullscreen(
  el: Element = doc.documentElement
): Promise<void> | void {
  const req =
    el.requestFullscreen ||
    (el as any).webkitRequestFullscreen ||
    (el as any).mozRequestFullScreen ||
    (el as any).msRequestFullscreen;
  return req.call(el);
}

function exitFullscreen(): Promise<void> | void {
  const exit =
    doc.exitFullscreen ||
    (doc as any).webkitExitFullscreen ||
    (doc as any).mozCancelFullScreen ||
    (doc as any).msExitFullscreen;
  return exit.call(doc);
}

function isBrowserFullscreen() {
  return !!(
    doc.fullscreen ||
    (doc as any).fullscreenElement ||
    (doc as any).webkitFullscreenElement ||
    (doc as any).mozFullScreenElement ||
    (doc as any).msFullscreenElement ||
    (doc as any).webkitIsFullScreen ||
    (doc as any).mozFullScreen
  );
}

/**
 * Request fullscreen for element, or exit fullscreen if browser
 * is already in fullscreen.
 *
 * @param el defaults to `document.documentElement`
 *
 * @returns some modern browsers will return a promise
 */
function toggleFullscreen(el?: Element) {
  if (isBrowserFullscreen()) {
    return exitFullscreen();
  } else {
    return requestFullscreen(el);
  }
}

/**
 * Returns the current fullscreen state, and a toggle function if fullscreen is
 * available and enabled in the browser.
 */
export default function useFullscreen(): [
  boolean,
  typeof toggleFullscreen | undefined
] {
  const [isFullscreen, setState] = useState(isBrowserFullscreen());
  useEffect(() => {
    function handleFullscreenChange() {
      setState(isBrowserFullscreen());
    }
    if (fullscreenEnabled) {
      doc.addEventListener(changeEvent!, handleFullscreenChange);
      return () => {
        doc.removeEventListener(changeEvent!, handleFullscreenChange);
      };
    }
  }, []);
  return [isFullscreen, fullscreenEnabled ? toggleFullscreen : undefined];
}
