# React hook for the fullscreen API

A React hook that gives you the current fullscreen state, and a toggle function if
fullscreen is available and enabled in the browser.

Works in modern browsers and IE11.

```
import useFullscreen from 'use-fullscreen';

function Example() {
  const [isFullscreen, toggleFullscreen] = useFullscreen();
  return (
    <div>
      <h1>Is {!isFullscreen && <i>not</i>} fullscreen</h1>
      {toggleFullscreen ? (
        <button onClick={() => toggleFullscreen()}>Toggle fullscreen</button>
      ) : (
        <b>Fullscreen not supported :(</b>
      )}
    </div>
  );
}
```

[![Fullscreen example](https://codesandbox.io/static/img/play-codesandbox.svg)](https://codesandbox.io/s/ry935l71rp?view=preview)
